# Estimate distance of loci to nuclear membrane in yeasts

## Project description

User would like to identify fluorescent loci inside nuclei in yeasts and estimate its distance to the nuclear border marked by DAPI stain. This is done using on the maximum intensity project (MIP) of the Z-stack and for every time point.

### Project structure

- Data: folder containing images to use for analysis.
  - Channel 1: loci to estimate the distance for (red)
  - Channel 2: nuclei DAPI (green)
  - Channel 3: bright-field images of the yeasts (not used for this project)
- Output: folder containing all output generated from scripts in this project
- Scripts: folder containing the ImageJ macros described below

## Analysis

### `01_stardist_segment_nuclei.ijm`

> Requirements: please install Stardist into your Fiji installation following [instructions](https://imagej.net/plugins/stardist#installation).

This first script simply processes all the images contained in a user-defined input folder. It create the MIP for the DAPI channel and uses StarDist to create a label mask for every time point.

### Semi-automatic tracking step

> Requirements: please install [TrackMate](https://imagej.net/plugins/trackmate/) into your Fiji installation. It should come with Fiji by default.

In this step we aim at tracking the StarDist labels exported in the previous step. To do so, use the label-image-detector built in TrackMate and track the labels in time following [this tutorial](https://imagej.net/plugins/trackmate/trackmate-label-image-detector). Export the tracked labels as a label image TIF file, with the same name as the original file and appended with "`_labels_tracked.tif`.

### `02_find_spots_and_distance_to_nucleus.ijm`

> Requirements: please install the `PTBIOP` plugin into your Fiji installation by using the Help &rarr; Update &rarr; Manage update sites &rarr; tick `PTBIOP` checkbox &rarr; apply changes.

This script aims to find the fluorescent loci in the MIP image and uses the tracked labels generated previously to estimate the distance between loci and nuclear border. This script will export into the output folder the length of each distance line and morphometrics descriptors of each nucleu per time frame.

### `03_create_overlay.ijm`

This simpler script is used to generate an overlay with the MIP of the two original channels (nuclei and loci), the segmented and tracked nuclei, the loci points and the distance line between points and nuclear borders. Once executed, the script will save in the output folder the overlay for every image present in the input folder.

Here you can see an example crop of the obtained overlay:

![overlay-crop](output_animation.gif)

### `stack_csv.sh`

> Requirements: please install csvkit to use this script

Small bash utility to concatenate all distances and morphometrics csvs into a single file.

## Instructions

Simply open the ImageJ macros in order by drag and dropping them into the main Fiji window and press Run. If some scripts require specific plugins or packages, they are described in the requirements of each section.

## Author

Marco Dalla Vecchia dallavem@igmbc.fr
