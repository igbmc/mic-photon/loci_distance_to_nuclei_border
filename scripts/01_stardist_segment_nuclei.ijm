// Ask user to define input directory
InputDirName = getDirectory("Choose Directory with images");
// Ask user to define output directory
OutputDirName = getDirectory("Choose Directory for output");

// Get list of files in folder
fileList = getFileList(InputDirName);

// Loop through images
for (i=0; i<fileList.length; i++) {
	// Only process it if it's a tif image
	if (endsWith(fileList[i], ".tif")) {
		// Open it
		open(InputDirName + fileList[i]);
		// Get name
		imgName = getTitle();
		// Assume all channels are together in a hyperstack --> split channels into separate images
		run("Split Channels");
		// Create MAX Intensity projection for nuclear segmentation
		selectImage("C2-" + imgName);
		run("Z Project...", "projection=[Max Intensity] all");
		// Close original images, we don't need them anymore
		close("C1-" + imgName);
		close("C2-" + imgName);
		close("C3-" + imgName);
		// Max projection creates image prepending 'MAX_' to original image name
		imgToSegmentName = "MAX_C2-" + imgName;
		// Actually segment with stardist
		StarDistSegment("MAX_C2-" + imgName);
		// Function above returns an image prepending 'labels-' to the image name
		selectImage("labels-" + "MAX_C2-" + imgName);
		// Export stardist segmentation to output folder
		saveAs("Tiff", OutputDirName + File.getNameWithoutExtension(imgName) + "_labels.tif");
		close("*");
	}
}

function StarDistSegment(imgName) { 
	// blurs input image and uses default stardist params to segment image
	// returns an image with 'labels-' prepended to image name
	selectImage(imgName);
	run("Gaussian Blur...", "sigma=2 stack");
	run("Command From Macro", "command=[de.csbdresden.stardist.StarDist2D], args=['input':'" + imgName + "', 'modelChoice':'Versatile (fluorescent nuclei)', 'normalizeInput':'true', 'percentileBottom':'1.0', 'percentileTop':'99.0', 'probThresh':'0.5', 'nmsThresh':'0.3', 'outputType':'Label Image', 'nTiles':'1', 'excludeBoundary':'2', 'roiPosition':'Automatic', 'verbose':'false', 'showCsbdeepProgress':'false', 'showProbAndDist':'false'], process=[false]");	
	selectImage("Label Image");
	rename("labels-" + imgName);
}