// Ask user to define input directory
InputDirName = getDirectory("Source Directory with images");
// Ask user to define output directory
OutputDirName = getDirectory("Destination Directory for output");

// Get list of files in folder
fileList = getFileList(InputDirName);

// Loop through images
for (i=0; i<fileList.length; i++) {
	// Only process it if it's a tif image
	if (endsWith(fileList[i], ".tif")) {
		// Open image and get its name
		open(InputDirName + fileList[i]);
		imgName = getTitle();
		// Assume all channels are together in a hyperstack --> split channels into separate images
		run("Split Channels");
		// Create MAX Intensity projection for both channels for visualization
		selectImage("C1-" + imgName);
		run("Z Project...", "projection=[Max Intensity] all");
		selectImage("C2-" + imgName);
		run("Z Project...", "projection=[Max Intensity] all");
		// close original images
		close("C1-" + imgName);
		close("C2-" + imgName);
		close("C3-" + imgName);
		// Enhance contrast for visualization (so signal is clearly visible)
		selectImage("MAX_C1-" + imgName);
		run("Enhance Contrast", "saturated=0.50");
		selectImage("MAX_C2-" + imgName);
		run("Enhance Contrast", "saturated=0.35");
		// Create a stack image with both enhances channels together
		run("Merge Channels...", "c1=[" + "MAX_C1-" + imgName +"] c2=[" + "MAX_C2-" + imgName + "] create");
		rename("Overlay-Stack");
		// Get number of frames from stack
		Stack.getDimensions(width, height, channels, slices, frames);
		// Loop through each frame
		for (i=1; i<=frames; i++) {
			selectImage("Overlay-Stack");
			// Set to current working frame
		    Stack.setFrame(i);
		    // Extract frame by duplicating it
		    run("Duplicate...", "title=" + "frame" + i);
		    selectImage("frame" + i);
		    // Flatten will cause the two channels to be squashes into a single RGB image
		    run("Flatten");
		    // open current frames ROIs we saved before
			roiManager("Open", OutputDirName + File.getNameWithoutExtension(imgName) + "_ROIs_frame_" + i + ".zip");
			// show all ROIs on current opened frame
			roiManager("Show All");
			// Flatten the ROIs overlay permanently into the RGB image
			run("Flatten");
			// change name because we will later stack all the overlays
			rename("Overlay-" + i);
			// close all images we don't need anymore
			close("frame" + i);
			close("frame" + i + "-1");
			close("ROI Manager");
		}
	}
	// close original 2 channels stack
	close("Overlay-Stack");
	// Use remaining images to create final overlay stack
	run("Images to Stack", "use");
	saveAs("Tiff", OutputDirName + File.getNameWithoutExtension(imgName) + "_overlay.tif");
}